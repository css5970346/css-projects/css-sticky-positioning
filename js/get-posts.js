export const getPosts = () => {
    fetch('https://dummyjson.com/posts')
        .then(res => res.json())
        .then(data => {
            const postList = document.getElementById('postList');
            data.posts.forEach(post => { // Change this line
                const listItem = document.createElement('article');
                listItem.innerHTML = `<h3>${post.title}</h3>
            <p>${post.body}</p>`
                postList.appendChild(listItem);
            });
        })
        .catch(error => console.error('Error:', error));
}